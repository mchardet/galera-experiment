---
- name: Backup MariaDB
  shell: "docker exec mariadb-container /usr/bin/mysqldump -u root --all-databases --password=my-secret-pw > backup.sql"
  tags: mariadb-backup
  when: inventory_hostname == dbmaster_node

- name: Destroying MariaDB container
  docker_container:
    name: "mariadb-container"
    state: absent
    force_kill: yes
  tags: mariadb-d0
  when: inventory_hostname == dbmaster_node

- name: Unmount /database
  mount:
    path: /database
    src: tmpfs
    fstype: tmpfs
    state: absent
  tags: mariadb-d1
  when: inventory_hostname == dbmaster_node

- name: Mount /database in tmpfs to store MariaDB
  mount:
    path: /database
    src: tmpfs
    fstype: tmpfs
    state: mounted
    opts: size=16G
  tags: mariadb-0

- name: Create MariaDB store and conf directories in /database
  file:
    path: "{{ item }}"
    state: directory
  with_items:
    - /database/data
    - /database/mysql.conf.d
  tags: mariadb-1

- name: Generate the configuration file
  template:
    src: mariadb-galera.conf.j2
    dest: /database/mysql.conf.d/mysql_server.cnf
  tags: mariadb-2

- name: Pull the MariaDB container
  docker_image:
      name: "mariadb:10.3.14"
  tags: mariadb-3

- name: Start the MariaDB-Galera first container
  docker_container:
    name: "mariadb-container"
    image: "mariadb:10.3.14"
    detach: True
    exposed_ports:
      - 4567
    published_ports:
      - "3306:3306"
      - "4567:4567"
      - "4567:4567/udp"
      - "4568:4568"
      - "4444:4444"
    volumes:
      - /database/mysql.conf.d:/etc/mysql/conf.d
      - /database/data:/var/lib/mysql
    env:
      MYSQL_INITDB_SKIP_TZINFO=yes
      MYSQL_ROOT_PASSWORD=my-secret-pw
    command: >
      --wsrep-new-cluster
      --wsrep_node_address="{{ hostvars[inventory_hostname]['ansible_' + database_network]['ipv4']['address'] }}"
  tags: mariadb-4-first
  when: inventory_hostname == dbmaster_node

#- name: Waiting for master MariaDB to be ready
#  wait_for:
#    host: "{{ hostvars[dbmaster_node]['ansible_' + database_network]['ipv4']['address'] }}"
#    port: 3306
#  tags: mariadb-5
#  when: inventory_hostname == dbmaster_node

- name: Waiting for master MariaDB to be ready
  command: >
    docker exec mariadb-container
    mysqladmin ping -u root -pmy-secret-pw
  register: result
  until: not result.rc  # or result.rc == 0 if you prefer
  retries: 120
  delay: 1
  tags: mariadb-5
  when: inventory_hostname == dbmaster_node
  
- name: Restore MariaDB
  shell: "cat backup.sql | docker exec -i mariadb-container /usr/bin/mysql -u root --password=my-secret-pw"
  retries: 120
  delay: 1
  register: result
  until: result is succeeded
  tags: mariadb-restore
  when: inventory_hostname == dbmaster_node

# The mariadb Docker image, by default, initializes a new database.
# But we don't want it to do so, since this is an active/active
# replication and database has been initialized during the previous
# task. To prevent the initialization, we `touch` the /var/lib/mysql
# directory.
- name: Create the /database/data/mysql directory to prevent MariaDB init
  file:
    path: /database/data/mysql
    state: directory
  tags: mariadb-4-other
  when: inventory_hostname != dbmaster_node

- name: Start MariaDB-Galera other containers
  docker_container:
    name: "mariadb-container"
    image: "mariadb:10.3.14"
    detach: True
    exposed_ports:
      - 4567
    published_ports:
      - "3306:3306"
      - "4567:4567"
      - "4567:4567/udp"
      - "4568:4568"
      - "4444:4444"
    volumes:
      - /database/mysql.conf.d:/etc/mysql/conf.d
      - /database/data:/var/lib/mysql
    command: --wsrep_node_address="{{ hostvars[inventory_hostname]['ansible_' + database_network]['ipv4']['address'] }}"
  tags: mariadb-4-other
  when: inventory_hostname != dbmaster_node

#- name: Waiting for worker MariaDB to be ready
#  wait_for:
#    host: "{{ hostvars[inventory_hostname]['ansible_' + database_network]['ipv4']['address'] }}"
#    port: 3306
#  tags: mariadb-5
#  when: inventory_hostname != dbmaster_node

- name: Waiting for worker MariaDB to be ready
  command: >
    docker exec mariadb-container
    mysqladmin ping -u root -pmy-secret-pw
  register: result
  until: not result.rc  # or result.rc == 0 if you prefer
  retries: 120
  delay: 1
  tags: mariadb-5
  when: inventory_hostname != dbmaster_node
