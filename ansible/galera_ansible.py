#!/usr/bin/env python

import time
from typing import Tuple


def time_test(g5k, nb_db_entries,
              printing: bool = False) -> Tuple[float, float, float, float]:
    from subprocess import run, CompletedProcess
    from json import dump

    if printing:
        print("Main: deploying")
    deploy_start_time: float = time.perf_counter()
    g5k.run_ansible(['ansible/deploy.yml'], extra_vars={
        'db': 'mariadb',
        'monitoring': False,
        'enos_action': 'deploy'
    })
    deploy_end_time: float = time.perf_counter()

    if nb_db_entries > 0:
        if printing:
            print("Main: sending database content")
        command = "cd db_builder/;python3 generate_db.py %d > ../data.sql" % nb_db_entries
        completed_process = run(command, shell=True, check=False)
        if completed_process.returncode is not 0:
            raise Exception("Error: Could not generate DB data")
        with open('data.sql') as sql_file:
            sql_data = sql_file.read()
        g5k.run_ansible(['ansible/senddatatodb.yml'], tags=["send-data-to-db"], extra_vars={"data": sql_data})
    else:
        open('data.sql', "a").close()  # create an empty file to be sure it is there
        if printing:
            print("Main: not sending database content (0 entries)")

    if printing:
        print("Main: waiting 5 seconds before reconfiguring")
    time.sleep(5)

    if printing:
        print("Main: reconfiguring to Galera")

    reconf_start_time: float = time.perf_counter()
    g5k.run_ansible(['ansible/reconfigure.yml'], extra_vars={
        'db': 'mariadb',
        'monitoring': False,
        'enos_action': 'reconfigure'
    })
    reconf_end_time: float = time.perf_counter()

    reconf2_start_time: float = 0.
    reconf2_end_time: float = 0.
    if len(g5k.get_hosts_info(role='database_add')) > 0:
        if printing:
            print("Main: waiting 5 seconds before second reconf")
        time.sleep(5)

        if printing:
            print("Main: adding %d Galera nodes" % len(g5k.get_hosts_info(role='database_add')))
        reconf2_start_time = time.perf_counter()
        g5k.run_ansible(['ansible/reconfigure2.yml'], extra_vars={
            'db': 'mariadb',
            'monitoring': False,
            'enos_action': 'reconfigure2'
        })
        reconf2_end_time = time.perf_counter()

    total_deploy_time = deploy_end_time - deploy_start_time
    total_reconf_time = reconf_end_time - reconf_start_time
    total_reconf2_time = reconf2_end_time - reconf2_start_time
    total_time = total_deploy_time + total_reconf_time + total_reconf2_time
    if printing:
        print("Total time in seconds: %f (deploy: %f, reconf: %f, reconf2: %f)" % (
            total_time, total_deploy_time, total_reconf_time, total_reconf2_time)
        )

    results = {"total_time": total_time, "total_deploy_time": total_deploy_time, "total_reconf_time": total_reconf_time}
    if len(g5k.get_hosts_info(role='database_add')) > 0:
        results["total_reconf2_time"] = total_reconf2_time
    with open("times.json", "w") as results_file:
        dump(results, results_file)
    return total_time, total_deploy_time, total_reconf_time, total_reconf2_time


def load_config(conf_file_location):
    from yaml import load
    try:
        from yaml import CLoader as Loader
    except ImportError:
        from yaml import Loader
    with open(conf_file_location, "r") as file:
        conf = load(file)
    return conf


if __name__ == '__main__':
    config = load_config("ansible_config.yaml")
    nb_db_entries = config["nb_db_entries"]

    from experiment_utilities.reserve_g5k import G5kReservation

    with G5kReservation(config, force_deployment=False, destroy=False, inventory_path="inventory.ini") as g5k_res:
        time_test(
            g5k_res, nb_db_entries,
            printing=True,
        )
